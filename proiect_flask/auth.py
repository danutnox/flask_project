from flask import Blueprint, render_template, redirect, url_for, request, flash
from werkzeug.security import generate_password_hash, check_password_hash
from proiect_flask.models import User
from __init__ import db
from flask_login import login_user, logout_user, login_required
from proiect_flask.models import User, Location, Job, Company

auth = Blueprint('auth', __name__)


@auth.route('/login')
def login():
    return render_template('login.html')


@auth.route('/signup')
def signup():
    return render_template('signup.html')


@auth.route('/logout')
@login_required
def logout():
    logout_user()
    return redirect(url_for('main.index'))


@auth.route('/signup', methods=['POST'])
def signup_post():
    email = request.form.get('email')
    name = request.form.get('name')
    password = request.form.get('password')

    user = User.query.filter_by(email=email).first()
    if user:
        print(email)
        flash('Email already exists')
    elif email is None:
        flash('You need to insert a valid email')
    elif name is None:
        flash("You need to insert a valid name")
    elif password is None:
        flash('Please insert a valid password')
    else:
        new_user = User(email=email, name=name, password=generate_password_hash(password))

        db.session.add(new_user)
        db.session.commit()

        print(email)
        return redirect(url_for('auth.login'))

    return redirect(url_for('auth.signup'))


@auth.route('/login', methods=['POST'])
def login_post():
    email = request.form.get('email')
    password = request.form.get('password')
    remember = True if request.form.get('remember') else False

    user = User.query.filter_by(email=email).first()

    if not user or not check_password_hash(user.password, password):
        flash('Please check your login details and try again.')
        return redirect(url_for('auth.login'))
    login_user(user, remember=remember)
    return redirect(url_for('main.profile'))


@auth.route('/job')
@login_required
def jobs():
    return render_template('jobs.html')


@auth.route('/company')
@login_required
def company():
    return render_template('companies.html')


@auth.route('/location')
@login_required
def location():
    return render_template('location.html')


@auth.route('/job', methods=['POST'])
@login_required
def jobs_post():
    job_type = request.form.get('type')
    job_url = request.form.get('url')
    job_title = request.form.get('title')
    job_description = request.form.get('description')
    job_how_to_apply = request.form.get('how_to_apply')

    job = Job(type=job_type, url=job_url, title=job_title, description=job_description, how_to_apply=job_how_to_apply)

    db.session.add(job)
    db.session.commit()
    return redirect(url_for('auth.company'))


@auth.route('/company', methods=['POST'])
@login_required
def company_post():
    company_name = request.form.get('name')
    company_web = request.form.get('website')
    company_logo = request.form.get('logo')

    company = Company(name=company_name, website=company_web, logo=company_logo, location_id=1)
    # if not company_name:
    #     flash('Please insert a company name')

    db.session.add(company)
    db.session.commit()
    return redirect(url_for("auth.location"))


@auth.route('/location', methods=['POST'])
@login_required
def location_post():
    job_location = request.form.get('location')

    location = Location(city=job_location)

    db.session.add(location)
    db.session.commit()
    return redirect(url_for('auth.jobs'))


f = open('proiect_flask/table.html', 'w+', encoding='utf-8')
f.write("""{% extends "base.html" %}
{% block content %}
<html><head><table><thead><tbody>""")
@auth.route('/table')
def tabel():
    content = Company.query.all()
    for x in content:
        f.write('<tr>{}</tr>'.format(x))
    return render_template('table.html')
f.write("""</tbody></thead></table></head></html>
{% endblock %}""")
f.close()